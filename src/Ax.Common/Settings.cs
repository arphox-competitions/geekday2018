﻿namespace Ax.Common
{
    public static class Settings
    {
        public const string TeamName = "AdvantageOrHandicap";

        public static class Map
        {
            public const int Columns = 11;
            public const int Rows = 9;
        }

        public static class Networking
        {
            public const string UdpIp = "192.168.1.67";
            public const int UdpPort = 1623;

            public static readonly string[] HttpPrefixes = 
            {
                $"http://{UdpIp}/homm/"
            };
        }

        public static class Logging
        {
            public const string LogPathBase = @"C:\temp\";

            public static readonly string HttpLogFilePath = $@"{LogPathBase}{Helper.CurrentDateTimeOnlySec}-HTTP.txt";
            public static readonly string UdpLogFilePath = $@"{LogPathBase}{Helper.CurrentDateTimeOnlySec}-UDP.txt";
            public static readonly string AiLogFilePath = $@"{LogPathBase}{Helper.CurrentDateTimeOnlySec}-AI.txt";
            public static readonly string BugLogFilePath = $@"{LogPathBase}{Helper.CurrentDateTimeOnlySec}-BUGS.txt";
        }
    }
}