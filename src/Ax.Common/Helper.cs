﻿using System;

namespace Ax.Common
{
    public static class Helper
    {
        public static string CurrentDateTimePrefixWithMs => DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.fff") + ": ";
        public static string CurrentDateTimePrefixOnlySec => DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.fff") + ": ";
        public static string CurrentDateTimeWithMs => DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.fff");
        public static string CurrentDateTimeOnlySec => DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.fff");
    }
}