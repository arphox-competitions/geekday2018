﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Ax.Networking.UDP
{
    public sealed class UdpListenerSettings
    {
        public IPEndPoint EndPoint { get; }
        public List<string> WhiteListedIps { get; }
        public bool IsWhiteListEnabled { get; }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UdpListenerSettings"/> class, accepting packets from every sender.
        /// </summary>
        public UdpListenerSettings(IPEndPoint endPoint)
        {
            EndPoint = endPoint ?? throw new ArgumentNullException(nameof(endPoint));
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UdpListenerSettings"/> class,
        ///     accepting packets from only a selected subset of IP addresses.
        /// </summary>
        public UdpListenerSettings(IPEndPoint endPoint, List<string> whiteListedIps)            
            :this(endPoint)
        {
            WhiteListedIps = whiteListedIps ?? throw new ArgumentNullException(nameof(whiteListedIps));

            if (WhiteListedIps.Count == 0)
                throw new ArgumentException($"{nameof(whiteListedIps)} cannot be empty.");

            IsWhiteListEnabled = true;
        }
    }
}