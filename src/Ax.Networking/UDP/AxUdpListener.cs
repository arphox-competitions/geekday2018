﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Ax.Networking.UDP
{
    public sealed class AxUdpListener : IDisposable
    {
        public event EventHandler<string> GeneralLog;
        public event EventHandler<byte[]> DataReceived;

        private readonly UdpListenerSettings listenerSettings;
        private UdpClient listener;

        public AxUdpListener(UdpListenerSettings listenerSettings)
        {
            this.listenerSettings = listenerSettings ?? throw new ArgumentNullException(nameof(listenerSettings));
        }

        public void Stop()
        {
            listener?.Close();
            listener?.Dispose();
            listener = null;
        }

        public void RunInBackground()
        {
            if (listener != null)
                throw new InvalidOperationException("Stop the listener first!");

            Action<byte[], IPEndPoint> handle = SelectHandler();

            Task.Factory.StartNew(
                () => Run(handle),
                TaskCreationOptions.LongRunning);
        }

        private Action<byte[], IPEndPoint> SelectHandler()
        {
            if (listenerSettings.IsWhiteListEnabled)
                return HandleDataWithWhiteList;
            else
                return HandleDataWithoutWhiteList;
        }

        private void Run(Action<byte[], IPEndPoint> handler)
        {
            listener = new UdpClient(listenerSettings.EndPoint);
            GeneralLog?.Invoke(this, $"UDP listener started listenning on port {listenerSettings.EndPoint.Port}.");
            IPEndPoint remoteEndpoint = null;

            while (true)
            {
                byte[] data = new byte[0];
                try
                {
                    data = listener.Receive(ref remoteEndpoint);
                }
                catch (SocketException e) when (e.ErrorCode == 10004)
                {
                    // 10004: "Interrupted function call. A blocking operation was interrupted by a call to WSACancelBlockingCall."
                    // -> this means we have been cancelled
                    break;
                }
                catch (Exception e)
                {
                    GeneralLog?.Invoke(this, $"Exception occured when handling an UDP data packet: {e}");
                    continue;
                }

                handler(data, remoteEndpoint);
            }

            GeneralLog?.Invoke(this, $"UDP listener STOPPED listenning on port {listenerSettings.EndPoint.Port}.");
        }

        private void HandleDataWithWhiteList(byte[] data, IPEndPoint remoteEndpoint)
        {
            string remoteIpAddress = remoteEndpoint.Address.ToString();
            if (!listenerSettings.WhiteListedIps.Contains(remoteIpAddress))
            {
                GeneralLog?.Invoke(this, "Denied an UDP message from non whitelisted IP: " + remoteIpAddress);
            }

            HandleDataWithoutWhiteList(data, remoteEndpoint);
        }

        private void HandleDataWithoutWhiteList(byte[] data, IPEndPoint remoteEndpoint)
        {
            string remoteIpAddress = remoteEndpoint.Address.ToString();
            // GeneralLog?.Invoke(this, $"Data received from IP '{remoteIpAddress}' with length of {data.Length}");
            DataReceived?.Invoke(this, data);
        }

        public void Dispose()
        {
            Stop();
        }
    }
}