﻿using Ax.Common;
using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.Map;
using Ax.Logic.Model.UDP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ax.Logic.Strategies
{
    public sealed class FlyStrategy : IStrategy
    {
        public event EventHandler<string> LogMessage;

        public bool CanApply(AiGameData gameData) => gameData.CurrentUnit.Unit.CanFly;

        public StepRequest TryApply(AiGameData gameData)
        {
            UdpData randomEnemy = gameData.EnemyUnits.GetRandomElement();
            gameData.AttackedTarget = randomEnemy;

            List<UdpData> neighbors = randomEnemy.GetNeighbors(gameData.Map.EntryMap);
            var positionGoTo = neighbors.FirstOrDefault(n => n.IsEmpty); // The first free slot next to the desired enemy

            if (positionGoTo == null)
            {
                // TODO: try new target?
                LogMessage?.Invoke(this, $"No free slots next to target: {randomEnemy.Unit.Name.ToString()} at X:{randomEnemy.X}, Y:{randomEnemy.Y}");
                return StepRequest.Empty;
            }

            return new StepRequest(positionGoTo.X, positionGoTo.Y, randomEnemy.ID);
        }
    }
}