﻿using System;
using Ax.Logic.Model.HTTP;

namespace Ax.Logic.Strategies
{
    public interface IStrategy
    {
        event EventHandler<string> LogMessage;
        bool CanApply(AiGameData gameData);
        StepRequest TryApply(AiGameData gameData);
    }
}