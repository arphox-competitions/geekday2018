﻿using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.UDP;
using System;
using System.Collections.Generic;
using Ax.Logic.Model.Map;

namespace Ax.Logic.Strategies
{
    public sealed class TestStrategy : IStrategy
    {
        public event EventHandler<string> LogMessage;

        public bool CanApply(AiGameData gameData) => gameData.CurrentUnit.Hero.IsRightSide;

        public StepRequest TryApply(AiGameData gameData)
        {
            UdpData currentUnit = gameData.CurrentUnit;
            UdpData nearestEnemy = gameData.EnemyUnits.GetNearest(gameData.CurrentUnit);
            gameData.AttackedTarget = null; // AttackThis is 0, so it is okay

            List<UdpData> neighbors = currentUnit.GetNeighbors(gameData.Map.EntryMap);
            UdpData positionToGo = neighbors.GetNearest(nearestEnemy);

            return new StepRequest(positionToGo.X, positionToGo.Y, 0);
        }
    }
}