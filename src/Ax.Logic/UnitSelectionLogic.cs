﻿using System;
using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.UDP;

namespace Ax.Logic
{
    public static class UnitSelectionLogic
    {
        public static UnitSelection GetUnitSelection(int squadMoney)
        {
            UnitName archmage = UnitName.Archmage;
            int archMagePrice = UnitPriceContainer.UnitPrices[archmage];
            UnitName titan = UnitName.Titan;
            int titanPrice = UnitPriceContainer.UnitPrices[titan];

            if (squadMoney < archMagePrice * 4) // under 2800 => only halflings
            {
                return OnlyHalflings(squadMoney);
            }

            if (squadMoney < titanPrice * 3) // under 16500 => only archmages
            {
                return OnlyArchmages(squadMoney);
            }

            // Add Titans and Archmages mixed

            UnitSelector selector = new UnitSelector(squadMoney);
            int howManyTitansCanWeBuy = squadMoney / titanPrice;
            if (howManyTitansCanWeBuy <= 5)
            {
                for (int i = 0; i < howManyTitansCanWeBuy; i++)
                    selector.Add(titan, 1);

                if (howManyTitansCanWeBuy < 5)
                {
                    int rem = selector.RemainingMoney;
                    if (rem > archMagePrice)
                        selector.Add(archmage, rem / archMagePrice);
                }

                return selector.GetSelection();
            }

            // We have more than 27500 money here
            int oneStackSize = howManyTitansCanWeBuy / 4;
            int remaining = howManyTitansCanWeBuy % 4;

            for (int i = 0; i < 4; i++)
            {
                if (remaining > 1)
                {
                    selector.Add(titan, oneStackSize + 1);
                    remaining--;
                }
                else
                {
                    selector.Add(titan, oneStackSize);
                }
            }

            int remainingMoney = selector.RemainingMoney;
            if (remainingMoney > archMagePrice)
                selector.Add(archmage, remainingMoney / archMagePrice);

            return selector.GetSelection();
        }

        private static UnitSelection OnlyArchmages(int squadMoney)
        {
            UnitSelector selector = new UnitSelector(squadMoney);

            UnitName archmage = UnitName.Archmage;
            int archMagePrice = UnitPriceContainer.UnitPrices[archmage];


            int howManyMagesCanWeBuy = squadMoney / archMagePrice;

            if (howManyMagesCanWeBuy <= 5)
            {
                for (int i = 0; i < howManyMagesCanWeBuy; i++)
                    selector.Add(archmage, 1);

                if (howManyMagesCanWeBuy < 5)
                {
                    UnitName halfling = UnitName.Halfling;
                    int halflingPrice = UnitPriceContainer.UnitPrices[halfling];
                    int rem = selector.RemainingMoney;
                    if (rem > halflingPrice)
                        selector.Add(halfling, rem / halflingPrice);
                }

                return selector.GetSelection();
            }

            // we can buy more than 5
            int oneStackSize;
            if (howManyMagesCanWeBuy % 4 == 0)
                oneStackSize = howManyMagesCanWeBuy / 5;
            else
                oneStackSize = howManyMagesCanWeBuy / 4;

            selector.Add(archmage, oneStackSize);
            selector.Add(archmage, oneStackSize);
            selector.Add(archmage, oneStackSize);
            selector.Add(archmage, oneStackSize);

            int remainingMoney = selector.RemainingMoney;
            selector.Add(archmage, remainingMoney / archMagePrice);

            return selector.GetSelection();
        }

        private static UnitSelection OnlyHalflings(int squadMoney)
        {
            UnitSelector selector = new UnitSelector(squadMoney);

            UnitName halfling = UnitName.Halfling;
            int halflingPrice = UnitPriceContainer.UnitPrices[halfling];

            int howManyCanWeBuy = squadMoney / halflingPrice;

            if (howManyCanWeBuy <= 5)
            {
                for (int i = 0; i < howManyCanWeBuy; i++)
                    selector.Add(halfling, 1);

                return selector.GetSelection();
            }

            // we can buy more than 5
            int oneStackSize;
            if (howManyCanWeBuy % 4 == 0)
                oneStackSize = howManyCanWeBuy / 5;
            else
                oneStackSize = howManyCanWeBuy / 4;

            selector.Add(halfling, oneStackSize);
            selector.Add(halfling, oneStackSize);
            selector.Add(halfling, oneStackSize);
            selector.Add(halfling, oneStackSize);

            int remainingMoney = selector.RemainingMoney;
            selector.Add(halfling, remainingMoney / halflingPrice);

            return selector.GetSelection();
        }
    }
}