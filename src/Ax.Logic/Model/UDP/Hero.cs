﻿using System;
using Newtonsoft.Json;

namespace Ax.Logic.Model.UDP
{
    [Serializable]
    public sealed class Hero
    {
        public bool IsLeftSide => !IsRightSide;

        /// <summary>
        /// Player data
        /// </summary>
        public Player Player { get; set; }

        /// <summary>
        /// # if True, the right player. if False, then left player
        /// </summary>
        [JsonProperty(PropertyName = "IsReverse")]
        public bool IsRightSide { get; set; }

        /// <summary>
        /// total, during current game
        /// </summary>
        public int DamageCaused { get; set; }

        /// <summary>
        /// total, during current game
        /// </summary>
        public float DistanceCovered { get; set; }

        /// <summary>
        /// current total HP of ALL units of the hero
        /// </summary>
        public int TotalHP { get; set; }
    }
}