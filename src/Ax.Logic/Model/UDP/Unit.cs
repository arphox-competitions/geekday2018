﻿using System;
using Newtonsoft.Json;

namespace Ax.Logic.Model.UDP
{
    [Serializable]
    public sealed class Unit
    {
        public bool IsRanged => CanShoot;

        public string Filename { get; set; }
        public Race Race { get; set; }
        public int Rank { get; set; }
        public UnitName Name { get; set; }
        public int HP { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int DmgMin { get; set; }
        public int DmgMax { get; set; }
        public int Speed { get; set; }

        /// <summary>
        /// If ranged unit, this is the initial number of ammo
        /// </summary>
        [JsonProperty(PropertyName = "Shoots")]
        public int MaxShoots { get; set; }

        public bool CanShoot { get; set; }

        public int Price { get; set; }

        public Specials Specials { get; set; }

        public bool CanFly => Specials.HasFlag(Specials.Fly);
    }
}