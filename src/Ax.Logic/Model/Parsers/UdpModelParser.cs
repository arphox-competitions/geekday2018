﻿using Ax.Logic.Model.UDP;

namespace Ax.Logic.Model.Parsers
{
    public static class UdpModelParser
    {
        public static UdpData ParseUdpData(string dataInJson)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<UdpData>(dataInJson);
        }
    }
}