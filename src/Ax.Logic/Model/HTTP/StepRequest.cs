﻿using System;
using Ax.Common;
using Newtonsoft.Json;

namespace Ax.Logic.Model.HTTP
{
    public sealed class StepRequest
    {
        public static readonly StepRequest Empty = new StepRequest(0, 0, 0);
        private static Random Random = new Random();

        public int MoveX { get; }
        public int MoveY { get; }
        public int AttackThis { get; }

        public StepRequest(int moveX, int moveY, int attackThis)
        {
            MoveX = moveX;
            MoveY = moveY;
            AttackThis = attackThis;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public static StepRequest RandomStep()
        {
            int randx = Random.Next(0, Settings.Map.Columns);
            int randY = Random.Next(0, Settings.Map.Rows);

            return new StepRequest(randx, randY, 0);
        }
    }
}