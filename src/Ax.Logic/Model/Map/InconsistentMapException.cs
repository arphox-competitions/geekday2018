﻿using System;
using System.Runtime.Serialization;

namespace Ax.Logic.Model.Map
{
    [Serializable]
    internal class InconsistentMapException : ApplicationException
    {
        public InconsistentMapException()
        {
        }

        public InconsistentMapException(string message) : base(message)
        {
        }

        public InconsistentMapException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InconsistentMapException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}