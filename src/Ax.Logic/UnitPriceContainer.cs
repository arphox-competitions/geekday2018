﻿using Ax.Logic.Model.UDP;
using System;
using System.Collections.Generic;
using System.IO;

namespace Ax.Logic
{
    public static class UnitPriceContainer
    {
        private const string Path = "Resources/unitprices.txt";

        public static Dictionary<UnitName, int> UnitPrices { get; } = new Dictionary<UnitName, int>();

        static UnitPriceContainer()
        {
            string[] lines = File.ReadAllLines(Path);

            foreach (string line in lines)
            {
                string[] lineSplitted = line.Split(new[] { " - " }, StringSplitOptions.None);
                UnitName unitName = (UnitName)Enum.Parse(typeof(UnitName), lineSplitted[0]);
                int price = int.Parse(lineSplitted[1]);

                UnitPrices.Add(unitName, price);
            }
        }
    }
}