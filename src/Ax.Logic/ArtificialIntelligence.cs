﻿using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.Map;
using Ax.Logic.Model.UDP;
using System;
using Ax.Common;

namespace Ax.Logic
{
    public sealed class ArtificialIntelligence
    {
        private object lockObject = new object();
        public event EventHandler<string> LogMessage;

        private static Random Random = new Random();

        public Map Map {get;private set;} = new Map();
        private readonly ResponseLogic responseLogic;

        public ArtificialIntelligence()
        {
            responseLogic = new ResponseLogic();
            responseLogic.LogMessage += (sender, s) => LogMessage?.Invoke(sender, s);
        }

        public void ProcessUdpData(UdpData data) => Map.Update(data);

        public UnitSelection GetUnitSelection(int squadMoney)
        {
            Map = new Map();
            return UnitSelectionLogic.GetUnitSelection(squadMoney);
        }

        public StepRequest GetStepResponse(StanceData stanceData)
        {
            lock (lockObject)
            {
                Map.DeleteDeadUnits(stanceData);
                Map.ThrowIfInconsistent();

                Map mapSnapshot = Map.DeepClone();
                return responseLogic.GetStepResponse(mapSnapshot, stanceData);
            }
        }
    }
}