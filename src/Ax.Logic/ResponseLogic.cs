﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ax.Common;
using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.Map;
using Ax.Logic.Model.UDP;
using Ax.Logic.Strategies;
using Ax.Logic.Strategies.Ranged;

namespace Ax.Logic
{
    public sealed class ResponseLogic
    {
        private readonly object bugLogLock = new object();
        public event EventHandler<string> LogMessage;

        private IStrategy[] strategies;

        public ResponseLogic()
        {
            strategies = new IStrategy[]
            {
                new ShootNearestUnitStrategy()
            };

            foreach (var strat in strategies)
            {
                strat.LogMessage += (sender, s) => LogMessage?.Invoke(sender, s);
            }
        }

        public StepRequest GetStepResponse(Map map, StanceData stanceData)
        {
            if (stanceData.Theirs.Count == 0)
            {
                LogMessage?.Invoke(this, "No enemies, we won!");
                return StepRequest.Empty;
            }

            AiGameData gameData = new AiGameData();
            gameData.Map = map;
            gameData.StanceData = stanceData;
            gameData.CurrentUnit = GetCurrentUnit(map, stanceData);
            UdpData currentUnit = gameData.CurrentUnit;
            if (currentUnit == null || currentUnit.IsDead)
                return StepRequest.Empty;

            gameData.EnemyUnits = stanceData.Theirs.Select(s => map.CurrentAliveUnits.Single(u => u.ID == s)).ToList();

            foreach (IStrategy strategy in strategies)
            {
                if (strategy.CanApply(gameData))
                {
                    StepRequest request = strategy.TryApply(gameData);
                    if (request == StepRequest.Empty)
                        continue;

                    string strategyName = strategy.GetType().Name.ToUpper();
                    string message = $"[{strategyName}]: Sending {currentUnit.Unit.Name.ToString()} [{currentUnit.X},{currentUnit.Y}] to [{request.MoveX},{request.MoveY}]";
                    if (request.AttackThis != 0)
                    {
                        var target = gameData.AttackedTarget;
                        message += $" and to attack {target.Unit.Name.ToString()} ({target.ID}) at [{target.X},{target.Y}]";
                    }

                    LogMessage?.Invoke(this, message);
                    return request;
                }
            }

            LogMessage?.Invoke(this, "No strategy could be applied, sending random request.");
            return StepRequest.RandomStep();
        }

        private UdpData GetCurrentUnit(Map map, StanceData stanceData)
        {
            UdpData currentUnit = map.CurrentAliveUnits.SingleOrDefault(u => u.ID == stanceData.Current);

            if (currentUnit == null)
            {
                List<MapEntry> list = map.EntryMap.Cast<MapEntry>().Where(e => e.UdpData.ID == stanceData.Current).ToList();
                List<UdpData> list2 = map.AllEverSeenDatas.Where(e => e.ID == stanceData.Current).ToList();
                string msg = Helper.CurrentDateTimePrefixWithMs + $"StanceData bug: '{stanceData}'";
                Task.Run(() =>
                {
                    lock (bugLogLock)
                    {
                        File.AppendAllText(Settings.Logging.BugLogFilePath, msg + Environment.NewLine);
                    }
                    LogMessage?.Invoke(this, msg);
                });
                LogMessage?.Invoke(this, $"Current unit (ID:{stanceData.Current}) is not found in our map, skipping this answer.");
            }
            
            return currentUnit;
        }
    }
}