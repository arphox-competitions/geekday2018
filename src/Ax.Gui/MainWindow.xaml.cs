﻿using Ax.Common;
using Ax.Logic;
using Ax.Logic.Model.HTTP;
using Ax.Logic.Model.Parsers;
using Ax.Logic.Model.UDP;
using Ax.Networking.HTTP;
using Ax.Networking.UDP;
using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Ax.Gui
{
    public partial class MainWindow : Window
    {
        private AxUdpListener udpListener;
        private AxHttpListener httpListener;

        private readonly ArtificialIntelligence ai = new ArtificialIntelligence();
        private MapWindow mapWindow;

        public MainWindow()
        {
            InitializeComponent();

            ai.LogMessage += (sender, message) =>
            {
                message = Helper.CurrentDateTimePrefixWithMs + message;
                Dispatcher.Invoke(() => { listBox_aiLog.Items.Insert(0, message); });
                FileLogger.LogAiToFile(message);
            };
            
            StartUdp();
            StartHttp();
            mapWindow = new MapWindow(ai);
            mapWindow.Show();
        }

        private void StartUdp()
        {
            UdpListenerSettings listenerSettings = new UdpListenerSettings(new IPEndPoint(IPAddress.Any, Settings.Networking.UdpPort));
            udpListener = new AxUdpListener(listenerSettings);

            udpListener.GeneralLog += (sender, message) => LogInfo(message);
            udpListener.GeneralLog += (sender, message) => LogUdpToFileWithPrecheck(message);

            udpListener.DataReceived += (sender, bytes) => LogUdpToFileWithPrecheck(Encoding.UTF8.GetString(bytes));
            udpListener.DataReceived += (sender, bytes) =>
            {
                try
                {
                    string dataInString = Encoding.UTF8.GetString(bytes);
                    UdpData udpData = UdpModelParser.ParseUdpData(dataInString);

                    Task.Run(() => Dispatcher.Invoke(() => UpdateStatistics(udpData)));

                    ai.ProcessUdpData(udpData);
                }
                catch (Exception e)
                {
                    LogInfo("--------------------------------------------------------------");
                    LogInfo("Exception occured while processing UDP data: " + e);
                    LogInfo("--------------------------------------------------------------");
                }
            };

            udpListener.RunInBackground();
        }

        private void StartHttp()
        {
            httpListener = new AxHttpListener(TryRespondToHtml, Settings.Networking.HttpPrefixes);
            httpListener.GeneralLog += (sender, message) => LogInfo(message);
            httpListener.GeneralLog += (sender, message) => FileLogger.LogHttpToFile(message);
            httpListener.Run();
        }

        private string TryRespondToHtml(HttpListenerRequest request)
        {
            try
            {
                return RespondToHtml(request);
            }
            catch (Exception e)
            {
                LogInfo("--------------------------------------------------------------");
                LogInfo("Exception occured while processing HTTP request: " + e);
                LogInfo("--------------------------------------------------------------");
                return "{}";
            }
        }

        private string RespondToHtml(HttpListenerRequest request)
        {
            string requestLogMessage = Helper.CurrentDateTimePrefixWithMs + "REQUEST: " + request.RawUrl;
            Task.Run(() =>
            {
                LogHttpReqResp(requestLogMessage);
            });
            FileLogger.LogHttpToFile(requestLogMessage);

            string response;

            int? squadMoney = TryGetSquadMoney(request);
            if (squadMoney != null)
            {
                response = HandleSquadMoney(squadMoney.Value);
            }
            else
            {
                StanceData stepRequest = StanceData.Parse(request.QueryString);
                LogInfo("Received StanceData: " + stepRequest);
                response = HandleStepRequest(stepRequest);
            }

            string responseLogMessage = Helper.CurrentDateTimePrefixWithMs + "RESPONSE: " + response;
            Task.Run(() =>
            {
                LogHttpReqResp(responseLogMessage);
            });
            FileLogger.LogHttpToFile(requestLogMessage);
            return response;
        }

        private static int? TryGetSquadMoney(HttpListenerRequest request)
        {
            if (!IsSquadMoney(request.QueryString.AllKeys))
                return null;

            return int.Parse(request.QueryString["squadMoney"]);

            //--------------------------------------------
            bool IsSquadMoney(string[] queryStrings)
            {
                if (queryStrings.Length != 1)
                    return false;

                return queryStrings[0] == "squadMoney";
            }
        }

        private string HandleSquadMoney(int value)
        {
            string json = ai.GetUnitSelection(value).ToJson();
            string message = "RESPONSE: UnitSelection: " + json;
            Task.Run(() =>
            {
                LogHttpReqResp(message);
            });
            FileLogger.LogHttpToFile(message);

            return json;
        }

        private string HandleStepRequest(StanceData stanceData)
        {
            string json = ai.GetStepResponse(stanceData).ToJson();
            string message = "RESPONSE: StepRequest: " + json;
            Task.Run(() =>
            {
                LogHttpReqResp(message);
            });
            FileLogger.LogHttpToFile(message);

            return json;
        }

        private void LogHttpReqResp(string message)
        {
            Dispatcher.Invoke(() => { listBox_httpReqRespLog.Items.Insert(0, Helper.CurrentDateTimePrefixWithMs + message); });
        }

        private void LogInfo(string message)
        {
            Dispatcher.Invoke(() => { listBox_infoLog.Items.Insert(0, Helper.CurrentDateTimePrefixWithMs + message); });
        }

        private void LogUdpToFileWithPrecheck(string message)
        {
            bool isChecked = false;
            Dispatcher.Invoke(() => { isChecked = checkBox_shouldLogRawUdpToFile.IsChecked.Value; });
            if (!isChecked)
                return;

            FileLogger.LogUdpToFile(message);
        }

        private void UpdateStatistics(UdpData udpData)
        {
            if (udpData.Hero.IsRightSide)
            {
                label_h2Name.Content = udpData.Hero.Player.Name;
                label_h2dam.Content = udpData.Hero.DamageCaused;
                label_h2dist.Content = udpData.Hero.DistanceCovered;
                label_h2hp.Content = udpData.Hero.TotalHP;
                label_h2lu.Content = Helper.CurrentDateTimeWithMs;
            }
            else
            {
                label_h1Name.Content = udpData.Hero.Player.Name;
                label_h1dam.Content = udpData.Hero.DamageCaused;
                label_h1dist.Content = udpData.Hero.DistanceCovered;
                label_h1hp.Content = udpData.Hero.TotalHP;
                label_h1lu.Content = Helper.CurrentDateTimeWithMs;
            }
        }

        private void RawUdpToggleButton_Click(object sender, RoutedEventArgs e)
        {
            UdpLogWindow logWindow = new UdpLogWindow();
            udpListener.DataReceived += (o, bytes) => logWindow.Log(bytes);
            logWindow.Show();
        }

        private void ButtonClearUiLogs_Click(object sender, RoutedEventArgs e)
        {
            listBox_infoLog.Items.Clear();
            listBox_aiLog.Items.Clear();
            listBox_httpReqRespLog.Items.Clear();
        }
    }
}